<?php

namespace Drupal\http_client_error_status;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Main http_client_error_status service dealing with block instances.
 */
final class Main {

  use StringTranslationTrait;

  /**
   * Constructs a Move object.
   */
  public function __construct(
    private readonly EntityTypeManagerInterface $entityTypeManager,
  ) {}

  /**
   * Removes all instances of the plugin from blocks.
   *
   * This allows for the blocks to remain in place, rather than being deleted.
   */
  public function removePluginInstances(): void {

    /** @var \Drupal\Core\Entity\EntityStorageInterface $block_storage */
    $block_storage = $this->entityTypeManager->getStorage('block');

    /** @var \Drupal\block\Entity\Block[] $blocks */
    $blocks = $block_storage->loadMultiple();

    foreach ($blocks as $block) {
      $conditions = $block->getVisibilityConditions();
      // If condition set, remove condition.
      if ($conditions->has('http_client_error_status')) {
        $conditions->removeInstanceId('http_client_error_status');
        $block->save();
      }
    }

  }

  /**
   * List instances of the plugin on blocks.
   *
   * @param bool $include_link
   *   Should the link to edit the block be included.
   */
  public function listPluginInstances(bool $include_link = TRUE): array {

    /** @var \Drupal\Core\Entity\EntityStorageInterface $block_storage */
    $block_storage = $this->entityTypeManager->getStorage('block');

    /** @var \Drupal\block\Entity\Block[] $blocks */
    $blocks = $block_storage->loadMultiple();

    $active_blocks = [];
    foreach ($blocks as $block) {
      $conditions = $block->getVisibilityConditions();

      if ($conditions->has('http_client_error_status')) {
        $block_conditions = $conditions->get('http_client_error_status')->getConfiguration();
        $block_id = $block->id();
        $potentialConflict = $this->checkBlockVisibilityConflict($block_id);

        $current_block = [
          'id' => $block->id(),
          'label' => $block->label(),
          'negate' => $block_conditions['negate'] ? $this->t('Yes') : $this->t('No'),
          'request_401' => $block_conditions['request_401'] ? $this->t('Yes') : $this->t('No'),
          'request_403' => $block_conditions['request_403'] ? $this->t('Yes') : $this->t('No'),
          'request_404' => $block_conditions['request_404'] ? $this->t('Yes') : $this->t('No'),
          'potential_conflict' => $potentialConflict ? $this->t('Yes') : $this->t('No'),
          'link' => Link::createFromRoute($this->t('Edit'), 'entity.block.edit_form', ['block' => $block->id()], ['query' => ['destination' => '/admin/config/development/http-client-error-status']])->toString(),
        ];

        if (!$include_link) {
          unset($current_block['link']);
        }

        $active_blocks[] = $current_block;
      }
    }

    return $active_blocks;

  }

  /**
   * Update the block visibility conditions.
   */
  public function updateBlockVisibility(): void {

    /** @var \Drupal\Core\Entity\EntityStorageInterface $block_storage */
    $block_storage = $this->entityTypeManager->getStorage('block');

    /** @var \Drupal\block\Entity\Block[] $blocks */
    $blocks = $block_storage->loadMultiple();

    foreach ($blocks as $block) {

      $block_id = $block->id();
      $potentialConflict = $this->checkBlockVisibilityConflict($block_id);
      // Skip to next block, if potential conflict with current.
      if ($potentialConflict) {
        continue;
      }

      $conditions = $block->getVisibilityConditions();

      if ($conditions->has('http_client_error_status')) {
        $block_conditions = $conditions->get('http_client_error_status')->getConfiguration();

        $new_response_status = $this->convertCondition($block_conditions);
        if (!empty($new_response_status)) {
          $block->setVisibilityConfig('response_status', $new_response_status);
        }

        $remaining_conditions = $this->remainingCondition($block_conditions);
        if (!empty($remaining_conditions)) {
          $block->setVisibilityConfig('http_client_error_status', $remaining_conditions);
        }

        $block->save();
      }
    }

  }

  /**
   * Check if the block visibility conditions conflict.
   *
   * @param mixed $block_id
   *   The block id.
   *
   * @return bool
   *   Return TRUE if potential block visibility conditions conflict.
   */
  public function checkBlockVisibilityConflict($block_id): bool {
    /** @var \Drupal\Core\Entity\EntityStorageInterface $block_storage */
    $block_storage = $this->entityTypeManager->getStorage('block');

    /** @var \Drupal\block\Entity\Block $block */
    $block = $block_storage->load($block_id);

    $conditions = $block->getVisibilityConditions();

    if ($conditions->has('http_client_error_status') &&
      $conditions->has('response_status')) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Convert the block visibility conditions to the core response status.
   *
   * @param array $block_conditions
   *   The block condition config.
   *
   * @return array
   *   The new config array.
   */
  private function convertCondition(array $block_conditions = []): array {

    // 403
    if ($block_conditions['request_403'] && !$block_conditions['request_404'] && !$block_conditions['negate']) {
      return [
        'id' => 'response_status',
        'negate' => FALSE,
        'status_codes' => [
          0 => '403',
        ],
      ];
    }

    // 404
    if (!$block_conditions['request_403'] && $block_conditions['request_404'] && !$block_conditions['negate']) {
      return [
        'id' => 'response_status',
        'negate' => FALSE,
        'status_codes' => [
          0 => '404',
        ],
      ];
    }

    // 200
    if ($block_conditions['request_403'] && $block_conditions['request_404'] && $block_conditions['negate']) {
      return [
        'id' => 'response_status',
        'negate' => FALSE,
        'status_codes' => [
          0 => '200',
        ],
      ];
    }

    // 200, 403
    if (!$block_conditions['request_403'] && $block_conditions['request_404'] && $block_conditions['negate']) {
      return [
        'id' => 'response_status',
        'negate' => FALSE,
        'status_codes' => [
          0 => '200',
          1 => '403',
        ],
      ];
    }

    // 200, 404
    if ($block_conditions['request_403'] && !$block_conditions['request_404'] && $block_conditions['negate']) {
      return [
        'id' => 'response_status',
        'negate' => FALSE,
        'status_codes' => [
          0 => '200',
          1 => '404',
        ],
      ];
    }

    // 403, 404
    if ($block_conditions['request_403'] && $block_conditions['request_404'] && !$block_conditions['negate']) {
      return [
        'id' => 'response_status',
        'negate' => FALSE,
        'status_codes' => [
          0 => '403',
          1 => '404',
        ],
      ];
    }

    return [];
  }

  /**
   * Rebuild the remaining block visibility conditions.
   *
   * @param array $block_conditions
   *   The block condition config.
   *
   * @return array
   *   The new config array.
   */
  private function remainingCondition(array $block_conditions = []): array {

    // Preserve 401 config, if used.
    if ($block_conditions['request_401']) {
      return [
        'id' => 'http_client_error_status',
        'negate' => $block_conditions['negate'],
        'request_401' => $block_conditions['request_401'],
        'request_403' => FALSE,
        'request_404' => FALSE,
      ];
    }

    return [
      'id' => 'http_client_error_status',
      'negate' => FALSE,
      'request_401' => FALSE,
      'request_403' => FALSE,
      'request_404' => FALSE,
    ];

  }

  /**
   * Get the table header for the block listing.
   *
   * @return array
   *   The header array.
   */
  public function tableHeader(): array {
    $header = [
      $this->t('Block ID'),
      $this->t('Block Title'),
      $this->t('Negated'),
      $this->t('Status Code - 401'),
      $this->t('Status Code - 403'),
      $this->t('Status Code - 404'),
      $this->t('Potential Conflict'),
      $this->t('Edit block instance'),
    ];

    return $header;
  }

}

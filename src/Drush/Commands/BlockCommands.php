<?php

declare(strict_types=1);

namespace Drupal\http_client_error_status\Drush\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drupal\http_client_error_status\Main;
use Drush\Attributes as CLI;
use Drush\Commands\AutowireTrait;
use Drush\Commands\DrushCommands;
use Symfony\Component\DependencyInjection\Attribute\Autowire;

/**
 * Drush commands for the http_client_error_status module.
 */
class BlockCommands extends DrushCommands {

  use AutowireTrait;

  /**
   * Constructs a BlockCommands object.
   *
   * @param \Drupal\http_client_error_status\Main $main
   *   The http_client_error_status service.
   */
  public function __construct(
    #[Autowire(service: 'http_client_error_status.main')] protected Main $main,
  ) {
    $this->main = $main;
  }

  /**
   * Removes all instances of the plugin from blocks.
   *
   * This allows for the blocks to remain in place, rather than being deleted.
   */
  #[CLI\Command(name: 'http_client_error_status:remove', aliases: ['hces:remove', 'hces-remove'])]
  #[CLI\Usage(name: 'drush http_client_error_status:remove', description: "Removes all instances of the plugin from all blocks.")]
  public function removePluginInstances(): void {
    if (!$this->io()->confirm('This will remove all instances of the plugin http_client_error_status from blocks, so might change visibility settings. Do you wish to proceed?')) {
      $this->io()->warning('Operation aborted.');
      return;
    }
    $this->main->removePluginInstances();
    $this->io()->success('All instances of the plugin http_client_error_status have been removed from blocks. Please check your block visibility settings.');
  }

  /**
   * List instances of the plugin on blocks.
   */
  #[CLI\Command(name: 'http_client_error_status:list', aliases: ['hces:list', 'hces-list'])]
  #[CLI\Usage(name: 'drush http_client_error_status:list', description: "Display a list instances of the plugin on all blocks.")]
  #[CLI\FieldLabels(
    labels: [
      'id' => 'Block ID',
      'label' => 'Block Title',
      'negate' => 'Negated',
      'request_401' => 'Status Code - 401',
      'request_403' => 'Status Code - 403',
      'request_404' => 'Status Code - 404',
      'potential_conflict' => 'Potential Conflict',
    ],
  )]
  #[CLI\DefaultTableFields(
    fields: [
      'id', 'label', 'negate', 'request_401', 'request_403', 'request_404', 'potential_conflict',
    ],
  )]
  public function listPluginInstances($options = ['format' => 'table']): RowsOfFields {

    $active_blocks = $this->main->listPluginInstances(FALSE);

    $data = new RowsOfFields($active_blocks);

    return $data;
  }

  /**
   * Update block visibility conditions to core settings.
   */
  #[CLI\Command(name: 'http_client_error_status:update', aliases: ['hces:update', 'hces-update'])]
  #[CLI\Usage(name: 'drush http_client_error_status:update', description: "Update block visibility conditions to use core settings.")]
  #[CLI\FieldLabels(
    labels: [
      'id' => 'Block ID',
      'label' => 'Block Title',
      'negate' => 'Negated',
      'request_401' => 'Status Code - 401',
      'request_403' => 'Status Code - 403',
      'request_404' => 'Status Code - 404',
      'potential_conflict' => 'Potential Conflict',
    ],
  )]
  #[CLI\DefaultTableFields(
    fields: [
      'id', 'label', 'negate', 'request_401', 'request_403', 'request_404', 'potential_conflict',
    ],
  )]
  public function updateBlockVisibility($options = ['format' => 'table']): mixed {
    $this->main->updateBlockVisibility();
    $this->io()->success('Block visibility conditions have been updated to core settings. If there were any conflicting core settings, they have been skipped, and are listed below.');
    $active_blocks = $this->main->listPluginInstances(FALSE);

    $data = new RowsOfFields($active_blocks);

    return $data;
  }

}

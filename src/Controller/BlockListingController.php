<?php

namespace Drupal\http_client_error_status\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\http_client_error_status\Main;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for HTTP Client Error Status Block Condition routes.
 */
final class BlockListingController extends ControllerBase {

  /**
   * The controller constructor.
   */
  public function __construct(
    private readonly Main $main,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('http_client_error_status.main'),
    );
  }

  /**
   * Builds the response.
   */
  public function __invoke(): array {

    $header = $this->main->tableHeader();
    $rows = $this->main->listPluginInstances(TRUE);

    return [
      '#theme' => 'http_client_error_status_table',
      '#data' => [
        '#type' => 'table',
        '#header' => $header,
        '#rows' => $rows,
        '#empty' => $this->t('No blocks found. You can uninstall the module.'),
      ],
    ];

  }

}

## CONTENTS OF THIS FILE

  - Introduction
  - Requirements
  - Installation
  - Configuration
  - Maintainers

## INTRODUCTION

The module provides a Block Condition plugin, checking the status code
for 40x range of client errors. This can be used be any modules that use
the Condition plugins.

Therefore, one simple use case would be to display a block on a 404 Not
Found page.

The supported HTTP Client errors page codes are:
* 401
* 403
* 404

## REQUIREMENTS

Use 8.x-1.x version for Drupal 8 versions < 8.7.7.
Use 8.x-2.x version for Drupal 8 versions >= 8.7.7 and Drupal 9.
Use 3.0.x version for all new Drupal projects.
Use 3.1.x version for Drupal 10.2 onwards.

## INSTALLATION

  - Install as you would normally install a contributed Drupal module.
    See:
    <https://www.drupal.org/docs/extending-drupal/installing-modules>
    for further information.

## CONFIGURATION

The Condition will be displayed by supporting modules.

The most common use case will be for the core Block system.

  - Configure a block under Administration > Structure > Block layout
  - A vertical tab called HTTP 40x Client errors will be displayed.
  - Choose the settings you require for the block. e.g. Check ‘Display
    on 403 Page’, if you want the block to be displayed on the HTTP 403
    Forbidden Client error page.

## UPDATING

When updating to 3.1.x you may need to clear caches to view the new page.
You will also need to be using drush 12.5.2 in order for the drush commands to 
be recognized.

## Version 3.1
This provides a manual upgrade path to use Drupal core block visibility settings
for status.
There are drush commands provided to assist with moving block visibility to use
core settings. However, it is suggested that you test this first, and deploy the
configuration, rather than doing this directly on a production site.

## MAINTAINERS

Current maintainers: [Jeff Logan (jefflogan)](https://www.drupal.org/u/jefflogan)

<?php

/**
 * @file
 * Post update functions for HTTP Client Error Status.
 */

/**
 * Clear cache for new template.
 */
function http_client_error_status_post_update_new_templates() {
  // Empty post_update hook.
}
